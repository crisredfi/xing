//
//  DataHandlerTest.m
//  Xing
//
//  Created by Aleix Segon on 21/01/2015.
//  Copyright (c) 2015 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "DataHandler.h"
#import "TestSemaphor.h"

@interface DataHandlerTest : XCTestCase
@property (nonatomic, strong) DataHandler *dataHandler;

@end

@implementation DataHandlerTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.dataHandler = [[DataHandler alloc] init];
}

- (void)testFetchRequest {
    
    [_dataHandler fetchdata:@"/jobs" withCompletitionBlock:^(NSArray *answer) {
        XCTAssertTrue([answer isKindOfClass:[NSArray class]], @"DataHandlerTest - answer is not array class, this will crash in TestFetchRequest");
        XCTAssertTrue([answer count] > 0, @"DataHandlerTest - Answer should be bigger than one for this test, something is going wrong in TestFetchRequest");
        [[TestSemaphor sharedInstance] lift:@"testFetchRequest"];

    } failBlock:^(NSError *error) {
        XCTFail(@"DataHandlerTest -  Fetch data just failed, something whent wrong trying to conect to the server");
        [[TestSemaphor sharedInstance] lift:@"testFetchRequest"];

    }];
    [[TestSemaphor sharedInstance] waitForKey:@"testFetchRequest"];

}


- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
