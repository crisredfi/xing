//
//  MasterViewControllerTest.m
//  Xing
//
//  Created by Aleix Segon on 22/01/2015.
//  Copyright (c) 2015 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "MasterViewController.h"
#import "JobsDataHandler.h"

// I will add some tests here. To fully test this properly we would need mock objects
// which i dont really think is the objective of this coding test to get that far.
// Im not willing to create a cocoapods project to use external libraries for the test
// and ocmock objects would require that.

@interface MasterViewControllerTest : XCTestCase
@property (nonatomic, strong) MasterViewController *masterViewController;

@end

@implementation MasterViewControllerTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _masterViewController = [[MasterViewController alloc] init];
}

- (void)testDelegateConforming {
    XCTAssertTrue([_masterViewController conformsToProtocol:@protocol(JobsDataHandlerDelegate)]);
}

- (void)testDelegateCall {
    // here we will only test if the delegate method is in here and if we call it with dummy data it works at all.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    
    XCTAssertTrue([_masterViewController respondsToSelector:@selector(jobsDataHandlerWillUpdateTableWithData:)]);
    
#pragma clang diagnostic pop
}

- (void)testSortButton {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    XCTAssertTrue([_masterViewController respondsToSelector:@selector(showSortDateOptions:)]);
#pragma clang diagnostic pop

}

- (void)testJobTypeButton {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    XCTAssertTrue([_masterViewController respondsToSelector:@selector(showJobCategoryOptions:)]);
#pragma clang diagnostic pop


}
- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
