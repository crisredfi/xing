//
//  JobTest.m
//  Xing
//
//  Created by Aleix Segon on 21/01/2015.
//  Copyright (c) 2015 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "Job.h"
#import "TestSemaphor.h"

@interface JobTest : XCTestCase

@end

@implementation JobTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)testJobInitialization {
    NSDictionary *dummyDict = @{@"id": @1,
    @"title": @"iOS Engineer at XING",
    @"job_type": @"FULL_TIME",
    @"created_at": @"2014-11-17T10:57:46Z",
                                @"logo_url": @"https://www.xing.com/img/custom/cp/assets/logo/2/6/8/616/logo/xing_web_72dpi_ohne_claim_gro%C3%9F.jpg"};
    Job *newJob = [[Job alloc] initWithData:dummyDict];
    
    XCTAssertTrue(newJob.identifier == 1, @"JobTest - identifier is incorrectly generated");
    XCTAssertTrue([newJob.title isEqualToString:@"iOS Engineer at XING"], @"JobTest - title is incorrectly generated");
    XCTAssertTrue([newJob.jobTypeString isEqualToString:@"FULL_TIME"], @"JobTest - jobTypeString is incorrectly generated");
    XCTAssertTrue(newJob.jobType == fullTime, @"JobTest - jobType is incorrectly generated");
    
    XCTAssertTrue([newJob.logoURL isEqualToString:@"https://www.xing.com/img/custom/cp/assets/logo/2/6/8/616/logo/xing_web_72dpi_ohne_claim_gro%C3%9F.jpg"] , @"JobTest - xing url is incorrectly generated");
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSString *stringFromDate = [dateFormatter stringFromDate:newJob.createDate];
    XCTAssertTrue([stringFromDate isEqualToString:@"2014-11-17T10:57:46Z"] , @"JobTest - NSDate is incorrectly generated");
    
}


- (void)testJobInitializationWithoutURL {
    NSDictionary *dummyDict = @{@"id": @1,
                                @"title": @"iOS Engineer at XING",
                                @"job_type": @"FULL_TIME",
                                @"created_at": @"2014-11-17T10:57:46Z"};
    Job *newJob = [[Job alloc] initWithData:dummyDict];
    
    XCTAssertTrue(newJob.identifier == 1, @"testJobInitializationWithoutURL - identifier is incorrectly generated");
    XCTAssertTrue([newJob.title isEqualToString:@"iOS Engineer at XING"], @"testJobInitializationWithoutURL - title is incorrectly generated");
    XCTAssertTrue([newJob.jobTypeString isEqualToString:@"FULL_TIME"], @"testJobInitializationWithoutURL - jobTypeString is incorrectly generated");
    XCTAssertTrue(newJob.jobType == fullTime, @"testJobInitializationWithoutURL - jobType is incorrectly generated");
    
    XCTAssertNil((newJob.logoURL), @"testJobInitializationWithoutURL - xing url is incorrectly generated");
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSString *stringFromDate = [dateFormatter stringFromDate:newJob.createDate];
    XCTAssertTrue([stringFromDate isEqualToString:@"2014-11-17T10:57:46Z"] , @"testJobInitializationWithoutURL - NSDate is incorrectly generated");
    
}



- (void)testJobInitializationWithoutData {
    NSDictionary *dummyDict = @{};
    Job *newJob = [[Job alloc] initWithData:dummyDict];
    
    XCTAssertTrue(newJob.identifier == 0 , @"testJobInitializationWithoutData - identifier is incorrectly generated");
    XCTAssertNil(newJob.title, @"testJobInitializationWithoutData - title is incorrectly generated");
    XCTAssertNil(newJob.jobTypeString, @"testJobInitializationWithoutData - jobTypeString is incorrectly generated");
    XCTAssertTrue(newJob.jobType == unknown, @"testJobInitializationWithoutData - jobType is incorrectly generated");
    
    XCTAssertNil((newJob.logoURL), @"testJobInitializationWithoutData - xing url is incorrectly generated");
    XCTAssertNil(newJob.createDate , @"testJobInitializationWithoutURL - NSDate is incorrectly generated");
    
}


- (void)testFetchImage {
    NSDictionary *dummyDict = @{@"id": @1,
                                @"title": @"iOS Engineer at XING",
                                @"job_type": @"FULL_TIME",
                                @"created_at": @"2014-11-17T10:57:46Z",
                                @"logo_url": @"https://www.xing.com/img/custom/cp/assets/logo/2/6/8/616/logo/xing_web_72dpi_ohne_claim_gro%C3%9F.jpg"};
    Job *newJob = [[Job alloc] initWithData:dummyDict];
    
    [newJob fetchImageForIndexPath:nil withCompletitionBlock:^(UIImage *image, NSIndexPath *imageIndexPath) {
        XCTAssertNotNil(image);
        [[TestSemaphor sharedInstance] lift:@"testFetchImage"];
    } withFailBlock:^(NSError *error, NSIndexPath *imageIndexPath) {
        XCTAssertNotNil(error);
        [[TestSemaphor sharedInstance] lift:@"testFetchImage"];
        
    }];
    [[TestSemaphor sharedInstance] waitForKey:@"testFetchImage"];

}


- (void)testFetchWithoutImage {
    NSDictionary *dummyDict = @{@"id": @1,
                                @"title": @"iOS Engineer at XING",
                                @"job_type": @"FULL_TIME",
                                @"created_at": @"2014-11-17T10:57:46Z"};
    Job *newJob = [[Job alloc] initWithData:dummyDict];
    
    [newJob fetchImageForIndexPath:nil withCompletitionBlock:^(UIImage *image, NSIndexPath *imageIndexPath) {
        XCTAssertNotNil(image);
        [[TestSemaphor sharedInstance] lift:@"testFetchWithoutImage"];
    } withFailBlock:^(NSError *error, NSIndexPath *imageIndexPath) {
        XCTAssertNotNil(error);
        [[TestSemaphor sharedInstance] lift:@"testFetchWithoutImage"];

    }];
    [[TestSemaphor sharedInstance] waitForKey:@"testFetchWithoutImage"];
    
}



- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
