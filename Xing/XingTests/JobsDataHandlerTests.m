//
//  JobsDataHandlerTests.m
//  Xing
//
//  Created by Aleix Segon on 22/01/2015.
//  Copyright (c) 2015 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "JobsDataHandler.h"
#import "Job.h"
#import "TestSemaphor.h"


@interface JobsDataHandlerTests : XCTestCase

@property (nonatomic, strong) JobsDataHandler *jobsDataHandler;

@end

@implementation JobsDataHandlerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _jobsDataHandler = [[JobsDataHandler alloc] initWithDelegate:nil];
}

// we cant test much this class since is using the delegate pattern for each response. If we
// implement blocks we could test it directly but


- (void)testSortJobs {
    
    [_jobsDataHandler sortJobs:nil withDateAscending:NO withCompletitionBlock:^(NSArray *answer) {
        XCTAssertNil(answer , @"testSortJobs - NSDate is incorrectly generated");
        [[TestSemaphor sharedInstance] lift:@"testSortJobsNil"];

    } failBlock:^(NSError *error) {
        [[TestSemaphor sharedInstance] lift:@"testSortJobsNil"];
    }];
    [[TestSemaphor sharedInstance] waitForKey:@"testSortJobsNil"];

    NSMutableArray *temporalArray = [NSMutableArray array];
    for (int i = 0; i < 6; i++) {
    
    
        NSString *dateString = [NSString stringWithFormat:@"2014-11-%iT10:57:46Z", i];
        NSDictionary *dummyDict = @{@"id": @(i),
                                    @"title": @"iOS Engineer at XING",
                                    @"job_type": @"FULL_TIME",
                                    @"created_at": dateString,
                                    @"logo_url": @"https://www.xing.com/img/custom/cp/assets/logo/2/6/8/616/logo/xing_web_72dpi_ohne_claim_gro%C3%9F.jpg"};
        Job *job = [[Job alloc] initWithData:dummyDict];
        [temporalArray addObject:job];
        

    }

    [self compareOrderedJobsWithTemporalArray:temporalArray withDateAscending:YES];
    [self compareOrderedJobsWithTemporalArray:temporalArray withDateAscending:NO];
    [self compareUnorderedJobsWithTemporalArray:temporalArray withDateAscending:YES];
    [self compareUnorderedJobsWithTemporalArray:temporalArray withDateAscending:NO];
    
}

- (void)compareOrderedJobsWithTemporalArray:(NSMutableArray *)temporalArray withDateAscending:(BOOL)isAscending  {
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"createDate" ascending:isAscending];
    NSArray *descriptors = @[descriptor];
    NSArray *ascending = [temporalArray sortedArrayUsingDescriptors:descriptors];
    
    [_jobsDataHandler sortJobs:temporalArray withDateAscending:isAscending withCompletitionBlock:^(NSArray *answer) {
        NSMutableSet *tempSet = [NSMutableSet setWithArray:answer];
        [tempSet addObjectsFromArray:temporalArray];
        NSUInteger tempSetCount = [tempSet count];
        NSUInteger tempArrCount = [temporalArray count];
        [answer enumerateObjectsUsingBlock:^(Job *obj, NSUInteger idx, BOOL *stop) {
            // we are comparing pointers here
            XCTAssertTrue((obj == ascending[idx]), @"testSortJobs - The output sorted array is different as expected");
        }];
        XCTAssertTrue((tempSetCount == tempArrCount), @"testSortJobs - The output sorted array has incorrect number of items");
        [[TestSemaphor sharedInstance] lift:@"testSortJobsAscending"];
    } failBlock:^(NSError *error) {
        XCTAssertNotNil(error);
        [[TestSemaphor sharedInstance] lift:@"testSortJobsAscending"];
        
    }];
    [[TestSemaphor sharedInstance] waitForKey:@"testSortJobsAscending"];
}


// this test will do a reverse sort and should fail no matter what. we will test here that the array is properly unsorted
// carefull here since some middle element might be equal. if temporal array input is pair, this wont happen, if its odd the middle
// object will be exacly the same and the test will fail. PASS ALLWAYS A PAIR NUMBER
- (void)compareUnorderedJobsWithTemporalArray:(NSMutableArray *)temporalArray withDateAscending:(BOOL)isAscending  {
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"createDate" ascending:!isAscending];
    NSArray *descriptors = @[descriptor];
    NSArray *ascending = [temporalArray sortedArrayUsingDescriptors:descriptors];
    
    [_jobsDataHandler sortJobs:temporalArray withDateAscending:isAscending withCompletitionBlock:^(NSArray *answer) {
        NSMutableSet *tempSet = [NSMutableSet setWithArray:answer];
        [tempSet addObjectsFromArray:temporalArray];
        NSUInteger tempSetCount = [tempSet count];
        NSUInteger tempArrCount = [temporalArray count];
        [answer enumerateObjectsUsingBlock:^(Job *obj, NSUInteger idx, BOOL *stop) {
            // we are comparing pointers here
            XCTAssertTrue((obj != ascending[idx]), @"testSortJobs - The output sorted array is different as expected");
        }];
        XCTAssertTrue((tempSetCount == tempArrCount), @"testSortJobs - The output sorted array has incorrect number of items");
        [[TestSemaphor sharedInstance] lift:@"testSortJobsAscending"];
    } failBlock:^(NSError *error) {
        XCTAssertNotNil(error);
        [[TestSemaphor sharedInstance] lift:@"testSortJobsAscending"];
        
    }];
    [[TestSemaphor sharedInstance] waitForKey:@"testSortJobsAscending"];
}



- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
