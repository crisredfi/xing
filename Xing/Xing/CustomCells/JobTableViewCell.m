//
//  JobTableViewCell.m
//  Xing
//
//  Created by Crisredfi on 20/01/15.
//  Copyright (c) 2015 crisredfi. All rights reserved.
//

#import "JobTableViewCell.h"

@implementation JobTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)tableView:(UITableView *)tableView didScrollCellOnView:(UIView *)view {
    // get the rect in superview. Superview is going to be the window since is teh visible part.
    CGRect rectInSuperview = [tableView convertRect:self.frame toView:view];
    float distFromCenter = CGRectGetHeight(view.frame)/2 - CGRectGetMinY(rectInSuperview);
    float difference = CGRectGetHeight(self.backgroundImage.frame) - CGRectGetHeight(self.frame);
    float move = (distFromCenter / CGRectGetHeight(view.frame)) * difference;
    
    CGRect imageRect = self.backgroundImage.frame;
    imageRect.origin.y   = -(difference/2) + move;

    self.backgroundImage.frame = imageRect;
    
}

@end
