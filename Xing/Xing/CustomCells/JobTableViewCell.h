//
//  JobTableViewCell.h
//  Xing
//
//  Created by Crisredfi on 20/01/15.
//  Copyright (c) 2015 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellLabel.h"
@interface JobTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet CellLabel *jobTitleLabel;
@property (weak, nonatomic) IBOutlet CellLabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet CellLabel *jobTypeLabel;

- (void)tableView:(UITableView *)tableView didScrollCellOnView:(UIView *)view;

@end
