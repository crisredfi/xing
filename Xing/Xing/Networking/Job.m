//
//  Job.m
//  Xing
//
//  Created by Crisredfi on 20/01/15.
//  Copyright (c) 2015 crisredfi. All rights reserved.
//

#import "Job.h"

@interface Job()
@property (nonatomic, strong) NSOperationQueue *queue;

@end

@implementation Job

- (instancetype)initWithData:(NSDictionary *)data
{
    self = [super init];
    if (self) {
        _identifier = [data[@"id"] unsignedIntegerValue];
        _title = data[@"title"];
        _jobTypeString = data[@"job_type"];
        _jobType = [self jobTypeFromString:_jobTypeString];
        _createDate = [self getDateFromString:data[@"created_at"]];
        _logoURL = data[@"logo_url"];
    }
    return self;
}


- (void)fetchImageForIndexPath:(NSIndexPath *)indexPath
         withCompletitionBlock:(void(^)(UIImage *image, NSIndexPath *imageIndexPath))block
                 withFailBlock:(void(^)(NSError *error, NSIndexPath *imageIndexPath))errorBlock
{
    if (!_queue) {
        self.queue = [[NSOperationQueue alloc] init];
    }
    if (_logoURL) {
        [self fetchImageFromString:_logoURL withCompletitionBlock:^(UIImage *image) {
            block(image, indexPath);
        } withFailBlock:^(NSError *error) {
            // lets return nil. we should show an alert or in background check for error habdling.
            errorBlock(error, indexPath);
        }];
    } else {
        // we should return a placeholder here...
        // by now we must return something. If not the Test will hang and fail
         NSError *cachedError = [[NSError alloc] initWithDomain:@"com.crisredfi.xingTest"
                                                           code:601
                                                       userInfo:@{@"error message": @"UIImage is nil, some data was corrupted or image url is incorrect"}];
        errorBlock(cachedError, indexPath);

        
    }
}


- (void)fetchImageFromString:(NSString *)stringUrl withCompletitionBlock:(void(^)(UIImage *image))block withFailBlock:(void(^)(NSError *error))errorBlock {
    NSURL *itemURL = [NSURL URLWithString:stringUrl];
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:itemURL]
                                       queue:_queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if (connectionError || !data || !data.length) {
                                   // we must return something. if this has failed, return this.
                                   // ideally we could add a fail block and add this as a failure.
                                   errorBlock(connectionError);
                                   return;
                               }
                               UIImage *cachedImage = [UIImage imageWithData:data];
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   if (cachedImage == nil) {
                                       NSError *cachedError = [[NSError alloc] initWithDomain:@"com.crisredfi.xingTest" code:600 userInfo:@{@"error message": @"UIImage is nil, some data was corrupted or image url is incorrect"}];
                                       errorBlock(cachedError);
                                   } else {
                                       block(cachedImage);
                                   }
                               });
                           }];
    
}

- (void)cancellImageDownlad {
    [_queue cancelAllOperations];
}

- (NSDate *)getDateFromString:(NSString *)input {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    return [dateFormatter dateFromString:input];
}

- (job_type)jobTypeFromString:(NSString *)input {
    
    NSString *lowerCaseInput = [input uppercaseString];
    if ([lowerCaseInput isEqualToString:@"FULL_TIME"]) {
        return fullTime;
    } else if ([lowerCaseInput isEqualToString:@"PART_TIME"]) {
    
        return partTime;
    } else if ([lowerCaseInput isEqualToString:@"STUDENT"]){
        return student;
    }
    return unknown;
}


@end
