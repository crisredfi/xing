//
//  DataHandler.m
//  Xing
//
//  Created by Crisredfi on 20/01/15.
//  Copyright (c) 2015 crisredfi. All rights reserved.
//

#import "DataHandler.h"

#define mainURL @"http://private-anon-5c9cdef49-xinginterview.apiary-mock.com"


@interface DataHandler ()<NSURLSessionDelegate>
@property (nonatomic, strong) NSURLSession *session;

@end


@implementation DataHandler

// not using a singleton since we only need 1 API call and we dont need a long
// live network connection. If not a singleton would be a good option.
// Also singleton are hard to Unit test and code injection is not what we want here..
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self startSession];
    }
    return self;
}


- (void)startSession {
    // code that sets up the default configuration for the NSURL session.
    // we set the code to accept jsons as it will be the main data receivers.
    // also set up timeout requests so we can handle timeouts.
    NSURLSessionConfiguration *sessionConfig =
    [NSURLSessionConfiguration defaultSessionConfiguration];
    
    [sessionConfig setHTTPAdditionalHeaders:@{ @"Accept" : @"application/json" }];
    
    sessionConfig.timeoutIntervalForRequest = 10.f;// ten secconds in mobile is a world
    sessionConfig.timeoutIntervalForResource = 30.0; // 30 secconds is just a full life...
    sessionConfig.HTTPMaximumConnectionsPerHost = 1;
    _session = [NSURLSession sessionWithConfiguration:sessionConfig
                                             delegate:self
                                        delegateQueue:nil];
}


- (void)fetchdata:(NSString *)data
withCompletitionBlock:(void (^)(NSArray *answer))block
        failBlock:(void (^)(NSError *error))blockError {
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", mainURL, data]];
    NSMutableURLRequest *request =
    [NSMutableURLRequest requestWithURL:url
                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                        timeoutInterval:10.0];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSURLSessionTask *dataTask =
    [_session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response,
                                    NSError *error) {
                    if (error) {
                        blockError(error);
                    } else {
                        NSArray *jsonArray =
                        [NSJSONSerialization JSONObjectWithData:data
                                                        options:kNilOptions
                                                          error:&error];
                        
                        block(jsonArray);
                    }
                }];
    [dataTask resume];
    
}




@end
