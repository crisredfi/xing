//
//  Job.h
//  Xing
//
//  Created by Crisredfi on 20/01/15.
//  Copyright (c) 2015 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, job_type) {
    fullTime,
    partTime,
    student,
    unknown
};

@interface Job : NSObject

@property (nonatomic, readonly, assign) NSUInteger identifier;
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *jobTypeString;
@property (nonatomic, readonly, assign) job_type jobType;
@property (nonatomic, readonly, strong) NSDate *createDate;
@property (nonatomic, readonly, copy) NSString *logoURL;
@property (nonatomic, readonly, strong) UIImage *image;
@property (nonatomic, strong) UIImage *dummyBackgroundImage; // only for pallalax dummy image effect


- (instancetype)initWithData:(NSDictionary *)data;
- (void)fetchImageForIndexPath:(NSIndexPath *)indexPath
         withCompletitionBlock:(void(^)(UIImage *image, NSIndexPath *imageIndexPath))block
                 withFailBlock:(void(^)(NSError *error, NSIndexPath *imageIndexPath))errorBlock;
- (void)cancellImageDownlad;

@end
