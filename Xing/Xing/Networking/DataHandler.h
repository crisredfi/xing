//
//  DataHandler.h
//  Xing
//
//  Created by Crisredfi on 20/01/15.
//  Copyright (c) 2015 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataHandler : NSObject

- (void)fetchdata:(NSString *)data
withCompletitionBlock:(void (^)(NSArray *answer))block
        failBlock:(void (^)(NSError *error))blockError;

@end
