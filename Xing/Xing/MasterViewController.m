//
//  MasterViewController.m
//  Xing
//
//  Created by Crisredfi on 20/01/15.
//  Copyright (c) 2015 crisredfi. All rights reserved.
//

#import "MasterViewController.h"
#import "DataHandler.h"
#import "Job.h"
#import "JobTableViewCell.h"
#import "NSDate+TimeAgo.h"
#import "JobsDataHandler.h"

#define kNavigationbarPadding 0


typedef NS_ENUM(NSInteger, ScrollDirection)  {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
};


@interface MasterViewController ()<UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, JobsDataHandlerDelegate>

@property (nonatomic, assign) CGFloat lastContentOffset;
@property (nonatomic, strong) NSArray *objects;
@property (nonatomic, weak) IBOutlet UIView *headView;
@property (nonatomic, strong) DataHandler *dataHandler;
@property (nonatomic, strong) NSDate *countdown;
@property (nonatomic, strong) JobsDataHandler *jobsDataHandler;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation MasterViewController {
    BOOL _isDragging;
    enum ScrollDirection _scrollDirection;

}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // initialize data for search bar animations.
    self.countdown = [NSDate distantPast];
    // initialize values for dynamic buttons view
    _isDragging = NO;
    _scrollDirection = ScrollDirectionNone;
    self.lastContentOffset = self.tableView.contentOffset.y;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    _jobsDataHandler = [[JobsDataHandler alloc] initWithDelegate:self];
    

}

// we want the status bar to be shown also in landscape.
- (BOOL)prefersStatusBarHidden
{
    return NO;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"JobTableViewCell";

    JobTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    Job *job = self.objects[indexPath.row];
    cell.jobTitleLabel.text = job.title;
    cell.jobTypeLabel.text = [NSString stringWithFormat:@"Type: %@",
     [[job.jobTypeString stringByReplacingOccurrencesOfString:@"_" withString:@" "] capitalizedString]];
    cell.dateLabel.text = [job.createDate timeAgo];
    if (!job.dummyBackgroundImage) {
        switch (indexPath.row % 3) {
            case 0:
                job.dummyBackgroundImage = [UIImage imageNamed:@"maldivi-0026-600x400.jpg"];
                break;
            case 1:
                job.dummyBackgroundImage = [UIImage imageNamed:@"600x400-princeville-sunset.jpg"];
                break;
            case 2:
                job.dummyBackgroundImage  = [UIImage imageNamed:@"Beach-600px-wide.jpg"];
            default:
                break;
        }
    }
    cell.backgroundImage.image = job.dummyBackgroundImage;
    
    if (job.image) {
        cell.logoImageView.image = job.image;
    } else {
        [job fetchImageForIndexPath:indexPath withCompletitionBlock:^(UIImage *image, NSIndexPath *imageIndexPath) {
            // check if current index path is the same as the image one.
            if (cell) {
                cell.logoImageView.image = image;
            }
        } withFailBlock:^(NSError *error, NSIndexPath *imageIndexPath) {
            //Handle the error here...
        }];
    }
    cell.logoImageView.contentMode = UIViewContentModeScaleAspectFit;

    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // calculate the height of the cell accourding tho its size.
    // for that we need to relayout the cell and get the size that
    // autolayout gives us.
    static JobTableViewCell *sizingCell = nil;
    static NSString *CellIdentifier = @"JobTableViewCell";
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    });
    
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}


- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1.0f; // Add 1.0f for the cell separator height
}


// tell the system an aproximate value for our cells
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
        return 300;
}


- (void)tableView:(UITableView *)tableView
didEndDisplayingCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath {
    // cancell image download to save resources.
    if (indexPath.row <= [_objects count] -1) {
        Job *item = _objects[indexPath.row];
        [item cancellImageDownlad];
    }
}


#pragma mark - scroll view delegates
// handle the parallax effect here and dynamic buttons view

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    _isDragging = YES;
}
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset {
    _isDragging = NO;
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // apply effect only in visible cells for better performance.
    NSArray *visibleCells = [self.tableView visibleCells];
    for (JobTableViewCell *cell in visibleCells) {
        [cell tableView:self.tableView didScrollCellOnView:self.view];
    }
    _scrollDirection = ScrollDirectionNone;
    if (self.lastContentOffset > scrollView.contentOffset.y) {
        _scrollDirection = ScrollDirectionUp;
    }
    else if (self.lastContentOffset < scrollView.contentOffset.y) {
        _scrollDirection = ScrollDirectionDown;
    }
    
    float difference  = self.lastContentOffset - scrollView.contentOffset.y;
    self.lastContentOffset = scrollView.contentOffset.y;
    int offset = 0;
    offset = difference;
    
    
    if (scrollView.contentOffset.y <= - kNavigationbarPadding) {
        // reached top,
        self.headView.layer.position = CGPointMake(self.headView.layer.position.x,
                                                   self.headView.bounds.size.height/2 + kNavigationbarPadding);
        self.countdown = [NSDate date];
        return;
    }
    
    if  ((self.headView.layer.position.y + offset) > (self.headView.bounds.size.height/2 + kNavigationbarPadding)) {
        offset = 1;
    }
    
    if ((self.headView.layer.position.y + offset) >= - self.headView.bounds.size.height &&
        (self.headView.layer.position.y + offset) <= self.headView.bounds.size.height/2 + kNavigationbarPadding &&
        ([[NSDate date] timeIntervalSinceReferenceDate] - [self.countdown timeIntervalSinceReferenceDate] > 1 )) {
        
        if (self.headView != nil) {
            self.headView.layer.position = CGPointMake(self.headView.layer.position.x, self.headView.layer.position.y + offset);
        } else
            if (self.headView.layer.position.y >= self.headView.bounds.size.height) {
                self.headView.layer.position =  CGPointMake(self.headView.layer.position.x, self.headView.bounds.size.height/2 + kNavigationbarPadding);
                // reset also now the position of the frame to be the same as the layer
                self.countdown = [NSDate date];
            } else if(self.headView.layer.position.y < 0) {
                self.headView.layer.position = CGPointMake(self.headView.layer.position.x, 0);
                self.countdown = [NSDate date];
                
            }
    }
}


#pragma mark - Jobs data handler delegate

- (void)jobsDataHandlerWillUpdateTableWithData:(NSArray *)newData {
    self.objects = newData;
    
    // we know we are in section 0. lets reload data with some animation
    [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:0]
                  withRowAnimation:UITableViewRowAnimationAutomatic];
    // this is because scroll view is reset at the beggining and some cells values are
    // screwed up
    [self scrollViewDidScroll:nil];
    

}


#pragma mark - sort Methods

- (IBAction)showSortDateOptions:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"How would you like to sort?"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Older First", @"Most Recent", nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];

}



- (IBAction)showJobCategoryOptions:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Which Job Cateogry do you want to see?"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Full Time", @"Part Time", @"Student", nil];
    actionSheet.tag = 2;
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // depending on wich action sheet we clicked at, do an action or another.
    // Actions sheets as well as UIAlertsView have been updated in iOS 8 so
    // they are not following this pattern anymore. Since we need to give support to both, we have to implement this old way anyway, if we where supporting only
    // ios 8, the newer version would be much cleaner (UIAlertController)
    if (actionSheet.tag == 1) {
        switch (buttonIndex) {
            case 0: {
                [_jobsDataHandler sortJobs:_objects withDateAscending:YES withCompletitionBlock:^(NSArray *answer) {
                    // reuse delegate callback to reload data.
                    [self jobsDataHandlerWillUpdateTableWithData:answer];
                } failBlock:^(NSError *error) {
                    // handle any possible error. here we ignore it but in real world some alert would bes shown.
                }];
                break;
            }
            case 1: {
                [_jobsDataHandler sortJobs:_objects withDateAscending:NO withCompletitionBlock:^(NSArray *answer) {
                    // reuse delegate callback to reload data.
                    [self jobsDataHandlerWillUpdateTableWithData:answer];
                } failBlock:^(NSError *error) {
                    // handle possible error. here we ignore it but in real world some alert would bes shown.
                }];
                break;
            }
            default:
                // if cancel, ignore the action
                break;
        }
    } else if (actionSheet.tag == 2) {
        if (buttonIndex < 3) {// cancel button
            [_jobsDataHandler filterCategories:buttonIndex withCompletitionBlock:^(NSArray *answer) {
                [self jobsDataHandlerWillUpdateTableWithData:answer];
            } failBlock:^(NSError *error) {
                // handle possible errorr. here we ignore it but in real world some alert would bes shown.
            }];
        }
    }
}

@end
