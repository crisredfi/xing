#import <UIKit/UIKit.h>

@interface NSDate (TimeAgo)
- (NSString *)timeAgo;
- (NSString *)timeOnly;
@end

