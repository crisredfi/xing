//
//  JobsDataHandler.h
//  Xing
//
//  Created by Aleix Segon on 21/01/2015.
//  Copyright (c) 2015 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Job.h"

@protocol JobsDataHandlerDelegate;

@interface JobsDataHandler : NSObject

@property (nonatomic, weak) id<JobsDataHandlerDelegate> delegate;

- (instancetype)initWithDelegate:(id<JobsDataHandlerDelegate>)delegate;

- (void)sortJobs:(NSArray *)currentJobs
withDateAscending:(BOOL)isAscending
withCompletitionBlock:(void (^)(NSArray *answer))block
       failBlock:(void (^)(NSError *error))failBlock;

- (void)filterCategories:(job_type)jobType
   withCompletitionBlock:(void (^)(NSArray *answer))block
               failBlock:(void (^)(NSError *error))failBlock;
@end

@protocol JobsDataHandlerDelegate <NSObject>

- (void)jobsDataHandlerWillUpdateTableWithData:(NSArray *)newData;

@end
