//
//  JobsDataHandler.m
//  Xing
//
//  Created by Aleix Segon on 21/01/2015.
//  Copyright (c) 2015 crisredfi. All rights reserved.
//
// im using the blocks pattern since is cleaner and nicier to test than the delegate one.
// Also this class is now pretending to be extendable and reusable so having the tests attached
// to the delegate view is not great. Using blocks will help to test properly this class, being
// isolated from any other class

#import "JobsDataHandler.h"
#import "DataHandler.h"

@interface JobsDataHandler()
@property (nonatomic, strong) DataHandler *dataHandler;
@property (nonatomic, strong) NSMutableArray *objects;
@end

@implementation JobsDataHandler

- (instancetype)initWithDelegate:(id<JobsDataHandlerDelegate>)delegate {
    self = [super init];
    if (self) {
        _delegate = delegate;
        _dataHandler = [[DataHandler alloc] init];
        _objects = [[NSMutableArray alloc] init];
        [self generateData];
    }
    return self;
}

- (void)generateData {
 
    [_dataHandler fetchdata:@"/jobs"
      withCompletitionBlock:^(NSArray *answer) {
          // we are stil in backgroudn thread.
          [self modelJobs:answer];
      } failBlock:^(NSError *error) {
          NSLog(@"error...");
      }];
    
}

- (void)sortJobs:(NSArray *)currentJobs
withDateAscending:(BOOL)isAscending
withCompletitionBlock:(void (^)(NSArray *answer))block
       failBlock:(void (^)(NSError *error))failBlock {
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"createDate" ascending:isAscending];
    NSArray *descriptors = @[descriptor];
    NSArray *newOrder = [currentJobs sortedArrayUsingDescriptors:descriptors];
    
    if (newOrder) {
        // return a  block with the new order data.
        block(newOrder);
    } else {
        NSError *cachedError = [[NSError alloc] initWithDomain:@"com.crisredfi.xingTest" code:602
                                                      userInfo:@{@"error message": @"sortJobs is wrong, some data was corrupted or image url is incorrect"}];
        failBlock(cachedError);
    }

}

- (void)filterCategories:(job_type)jobType
   withCompletitionBlock:(void (^)(NSArray *answer))block
               failBlock:(void (^)(NSError *error))failBlock {
    
    NSMutableArray *temporal = [NSMutableArray array];
    [_objects enumerateObjectsUsingBlock:^(Job *obj, NSUInteger idx, BOOL *stop) {
        if (obj.jobType == jobType) {
            [temporal addObject:obj];
        }
    }];
    
    // probably temporal will ever be nil.... but lets generate the error just in case we need it in a future.
    if (temporal) {
        block (temporal);
    } else {
        NSError *cachedError = [[NSError alloc] initWithDomain:@"com.crisredfi.xingTest" code:603
                                                      userInfo:@{@"error message": @"Filter categories is wrong, some data was corrupted or image url is incorrect"}];
        failBlock(cachedError);
    }
}





- (void)modelJobs:(NSArray *)conversion {
  
    [conversion enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Job *conversiond = [[Job alloc] initWithData:obj];
        [_objects addObject:conversiond];
    }];

    dispatch_async(dispatch_get_main_queue(), ^{
        // we are still in background thread.
        // trigger the delegate and copy the objects with unmutable form.
        [self.delegate jobsDataHandlerWillUpdateTableWithData:[_objects copy]];
      
        
    });
}




@end
