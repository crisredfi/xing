#import "NSDate+TimeAgo.h"

@implementation NSDate (TimeAgo)

#ifndef NSDateTimeAgoLocalizedStrings
#define NSDateTimeAgoLocalizedStrings(key) \
    NSLocalizedStringFromTable(key, @"NSDateTimeAgo", nil)
#endif

- (NSString *)timeAgo {
    NSDate *now = [NSDate date];
    double deltaSeconds = fabs([self timeIntervalSinceDate:now]);
    double deltaMinutes = deltaSeconds / 60.0f;
    
    if(deltaSeconds < 5) {
        return NSDateTimeAgoLocalizedStrings(@"now");
    } else if(deltaSeconds < 60) {
        return [NSString stringWithFormat:NSDateTimeAgoLocalizedStrings(@"%ds"), (int)deltaSeconds];
    } else 
    if(deltaSeconds < 120) {
        return NSDateTimeAgoLocalizedStrings(@"1m ago");
    } else if (deltaMinutes < 60) {
        return [NSString stringWithFormat:NSDateTimeAgoLocalizedStrings(@"%dm ago"), (int)deltaMinutes];
    } else if (deltaMinutes < 120) {
        return NSDateTimeAgoLocalizedStrings(@"1h ago");
    } else if (deltaMinutes < (24 * 60)) {
        return [NSString stringWithFormat:NSDateTimeAgoLocalizedStrings(@"%dh ago"), (int)floor(deltaMinutes/60)];
    } else if (deltaMinutes < (24 * 60 * 2)) {
        return NSDateTimeAgoLocalizedStrings(@"Yesterday");
    } else if (deltaMinutes < (24 * 60 * 7)) {
        return [NSString stringWithFormat:NSDateTimeAgoLocalizedStrings(@"%dd"), (int)floor(deltaMinutes/(60 * 24))];
    } else if (deltaMinutes < (24 * 60 * 14)) {
        return NSDateTimeAgoLocalizedStrings(@"1 week ago");
    } else if (deltaMinutes < (24 * 60 * 31)) {
        return [NSString stringWithFormat:NSDateTimeAgoLocalizedStrings(@"%d weeks ago"), (int)floor(deltaMinutes/(60 * 24 * 7))];
    } else if (deltaMinutes < (24 * 60 * 61)) {
        return NSDateTimeAgoLocalizedStrings(@"1 month ago");
    } else if (deltaMinutes < (24 * 60 * 365.25)) {
        return [NSString stringWithFormat:NSDateTimeAgoLocalizedStrings(@"%d months ago"), (int)floor(deltaMinutes/(60 * 24 * 30))];
    } else if (deltaMinutes < (24 * 60 * 731)) {
        NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
        [timeFormat setDateFormat:@"d MMM yyyy"];
        
        return [timeFormat stringFromDate:self];
    }

    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"d MMM yyyy"];
    
    return [timeFormat stringFromDate:self];

//    return [NSString stringWithFormat:NSDateTimeAgoLocalizedStrings(@"%d years ago"), (int)floor(deltaMinutes/(60 * 24 * 365))];
}

- (NSString *)timeOnly {

    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"h:mm a"];
    
    NSString *theTime = [timeFormat stringFromDate:self];
    
    return [theTime lowercaseString];
}

@end
